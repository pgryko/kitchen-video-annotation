# CV/AI exercise

Along with this document you are receiving a file (available here too: https://bit.ly/3miRzjy) containing a subset of the well-known, publicly available, EPIC-Kitchens Dataset (http://epic-kitchens.github.io). The selected subset contains videos of different persons performing two actions:
Take Bag
Open Bag

Video annotations are gathered in two main files:
train_action_classes.csv
val_action_classes.csv

Each of these refers to the video file sources (split into a sequence of images) that are contained within the “train” and “val” folders, respectively. CSV files are organized as follows:
uid: is the global sequence id
participant_id: is the ID of the person/actor that is performing the action of interest
video_id: is the subfolder in train/val that contains the frames of the action of interest
narration / start_timestamp / stop_timestamp: not used
start_frame / stop_frame: indices denoting the begin/end of the action. These are used as the suffix of the image filenames within the participant_id folder.
verb / verb_class / noun / noun_class / all_nouns / all_noun_classes: not used
action_class: action type (can be either take bag or open bag).
To clarify some relevant points, the files P01_05/frame_0000030627.jpg,  P01_05/frame_0000030628.jpg, P01_05/frame_0000030629.jpg,  P01_05/frame_0000030630.jpg, ………,  P01_05/frame_0000030681.jpg correspond to the frames representing the “take bag” action performed by participant P01 in the training set (row with uid=1 in train_action_classes.csv).
Tasks
Your task are:
To come up with an approach to tackle the action recognition problem considering such a specific dataset.
To define a method to load and parse the CSV files into appropriate data structures.
To define proper system validation scripts (both for performance assessment and for code sanity tests)
To provide all the scripts to run your proposed approach on the same data you have received.
Rules of the Game
There are no restrictions on programming languages (though python is preferred).
You can use/build on the top of any existing framework
You can re-use existing solutions or approaches (but keep in mind that originality and out of the box solutions might be a plus).

# Notes:
Action recognition is typically approached with a combination of a CNN and RNN - the CNN is used to classify the contents of individial images whilst the RNN is used to analyse time series data.
